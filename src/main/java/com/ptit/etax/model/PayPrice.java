package com.ptit.etax.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayPrice {
	private Long price;
}
