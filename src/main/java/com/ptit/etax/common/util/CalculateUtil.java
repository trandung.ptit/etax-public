package com.ptit.etax.common.util;

import com.ptit.etax.common.enums.IncomePriceRule;

import java.util.stream.Stream;

public class CalculateUtil {
	public static Long calculate(Long totalIncome, Integer numberOfDependents) {
		Long amount = totalIncome - 11000000l - (numberOfDependents * 4400000l);	// Giảm trừ bản thân
		if (amount <= 0 ) return 0l;
		return Stream.of(IncomePriceRule.values())
				.map(rule -> rule.priceOf(amount))
				.reduce(0l, Long::sum);
	}
}
