package com.ptit.etax.util;

import com.ptit.etax.common.util.CalculateUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCalculator {

	@Test
	public void testCalculateTax(){
		assertEquals(0, CalculateUtil.calculate(500000l,0));
		assertEquals(0, CalculateUtil.calculate(500001l,0));
		assertEquals(0, CalculateUtil.calculate(9999999l,0));
		assertEquals(0, CalculateUtil.calculate(10000000l,0));
		assertEquals(0, CalculateUtil.calculate(10000001l,0));
		assertEquals(450000, CalculateUtil.calculate(18000000l,0));
		assertEquals(450000, CalculateUtil.calculate(18000001l,0));
		assertEquals(2550000, CalculateUtil.calculate(32000000l,0));
		assertEquals(2550000, CalculateUtil.calculate(32000001l,0));
		assertEquals(7000000, CalculateUtil.calculate(52000000l,0));
		assertEquals(7000000, CalculateUtil.calculate(52000001l,0));
		assertEquals(14850000, CalculateUtil.calculate(80000000l,0));
		assertEquals(14850000, CalculateUtil.calculate(80000001l,0));
		assertEquals(14850000, CalculateUtil.calculate(80000001l,0));
	}
}
